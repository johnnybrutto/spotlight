
#ifndef _TEST_APP
#define _TEST_APP

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxTimeline.h"
#include "light.hpp"


class testApp : public ofBaseApp{

public:
    void drawGui(ofEventArgs & args);
    void setupGui();
	void setup();
	void update();
    void addTimeline();
    void updateLights(ofEventArgs & args);
    void dealWithKey(ofKeyEventArgs & args);
	void draw();
    void exit();
	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
    void dealWithKey(int key);
    void addAnimationToggle(int timeline);
    void onToggle( const void * sender, bool & value );

private:
    ofxPanel gui1;
    ofxPanel gui2;


    vector <ofxColorSlider> colors;
    vector <ofxFloatSlider> xPosition;
    vector <ofxFloatSlider> yPosition;
    vector <ofxFloatSlider> radius;
    vector <ofxToggle> on_off;
    map < int, vector <ofxToggle*> > animLight;
    vector <bool> lightsOn;
    bool hGui, hTimeline;
    
    ofxTimeline timeline;
    vector<ofxTimeline*> sublines;
    vector<Light*> lights;
    map<string, bool> panels;
    
    vector<float> testx, testy;
    int currf;

};

#endif
