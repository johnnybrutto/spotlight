//
//  light.cpp
//  emptyExample
//
//  Created by Joao Freire on 4/30/16.
//
//

#include "light.hpp"

void Light::setup(ofColor _color)
{
    color = _color;
    currframe = 0;
    currtimeline = "";
    isRunning = false;
    isNextTimeline = false;
}

void Light::runAnimation()
{
    currframe = 0;
    if(currtimeline == "" && timelineNames.size() > 0)
    {
        currtimeline = timelineNames[0];
        isRunning = true;
    }
}

void Light::stopAnimation()
{
    isRunning = false;
    currtimeline = "";
}

void Light::addTimeline(ofxTimeline *timeline)
{
   // timelines[timeline->getName()]
 //   timelines.push_back(vector_pos);
    for(int i = 0; i < timeline->getDurationInFrames(); i++)
    {
        ofPoint aux;
        aux.x = timeline->getValue("X", i);
        aux.y = timeline->getValue("Y", i);
        timelineValues[timeline->getName()].push_back(aux);

    }
    timelineNames.push_back(timeline->getName());
    int bla = 0;
    cout<<"add "<<timeline->getName()<<endl;
}

void Light::updateTimeline(ofxTimeline* timeline)
{
    if ( timelineValues.find(timeline->getName()) == timelineValues.end() )
    {
        // not found
    }
    else
    {
        timelineValues[timeline->getName()].clear();
        for(int i = 0; i < timeline->getDurationInFrames(); i++)
        {
            ofPoint aux;
            aux.x = timeline->getValue("X", i);
            aux.y = timeline->getValue("Y", i);
            timelineValues[timeline->getName()].push_back(aux);
        }
    }
    
}
void Light::removeTimeline(string name)
{
    int pos = 0;
    std::vector<string>::iterator position = std::find(timelineNames.begin(), timelineNames.end(), name);
    if (position != timelineNames.end() /*&& currtimeline != name*/) // == myVector.end() means the element was not found
    {
        for(int i = 0; i < timelineNames.size(); i++)
        {
            if(timelineNames[i] == name)
                pos = i;
                
        }
        timelineNames.erase(position);
    }
    cout<<"remove "<<name<<"in position "<<pos<<endl;
    
    std::map<char,int>::iterator it;

    timelineValues.erase(name);
    
    if(currtimeline == name)
    {
        if(timelineNames.size() > 0)
            currtimeline = timelineNames[0];
        else
            currtimeline = "";
        
        currframe = 0;
    }

if(timelineNames.size() == 0)
{
    stopAnimation();
}
    //timelineValues.clear();

}

ofPoint Light::getPos()
{
    isNextTimeline = false;
    if(currframe > timelineValues[currtimeline].size())
    {
        for(int i = 0; i < timelineNames.size(); i++)
        {
            if(currtimeline == timelineNames[i])
            {
                if( i+1 > timelineNames.size()-1)
                    currtimeline = timelineNames[0];
                else
                    currtimeline = timelineNames[i+1];
                
                break;
            }
        }
        
        currframe = 0;
        cout<<"playing "<<currtimeline<<endl;
        isNextTimeline = true;

    }
    
    
    ofPoint _pos = timelineValues[currtimeline][currframe];
    currframe++;

    return _pos;
}

void Light::draw()
{
    
}
