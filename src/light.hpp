//
//  light.hpp
//  emptyExample
//
//  Created by Joao Freire on 4/30/16.
//
//

#ifndef light_hpp
#define light_hpp

#include <stdio.h>
#include "ofxGui.h"
#include "ofxTimeline.h"

class Light
{
public:
    void setup(ofColor _color);
    void addTimeline(ofxTimeline *aux);
    void removeTimeline(string name);
    ofPoint getPos();
    void draw();
    void runAnimation();
    void stopAnimation();
    void updateTimeline(ofxTimeline* timeline);
    bool isRunning;
    bool isNextTimeline;

private:
    map<string, vector <ofPoint> > timelineValues;
    vector<string> timelineNames;
    int currframe;
    ofColor color;
    string currtimeline;
};



#endif /* light_hpp */
