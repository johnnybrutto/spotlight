
#include "testApp.h"
#define NUM_COLORS 5
//--------------------------------------------------------------
void testApp::setup()
{
    ofEnableAlphaBlending();
    ofSetCircleResolution(100);
    ofSetFrameRate(100);
    ofSetVerticalSync(true);
    ofEnableSmoothing();
    if(lights.size() == 0)
    {
    for(int i = 0; i < 6; i++)
    {
        Light *light = new Light();
        lights.push_back(light);
    }
    }
}

void testApp::setupGui()
{
    gui1.setup();
    gui2.setup();
    
    for(int i = 0; i < 6; i++)
    {
        bool auxb = false;
        lightsOn.push_back(auxb);
        ofxColorSlider aux;
        colors.push_back(aux);
        ofxFloatSlider aux1;
        xPosition.push_back(aux1);
        ofxFloatSlider aux2;
        yPosition.push_back(aux2);
        ofxFloatSlider aux3;
        radius.push_back(aux3);
        ofxToggle aux4;
        on_off.push_back(aux4);
    }
    
    colors[0].setup("color1", ofColor(100,100,140), ofColor(0,0),ofColor(255,255));
    colors[1].setup("color2", ofColor(100,100,140), ofColor(0,0),ofColor(255,255));
    colors[2].setup("color3", ofColor(100,100,140), ofColor(0,0),ofColor(255,255));
    colors[3].setup("color4", ofColor(100,100,140), ofColor(0,0),ofColor(255,255));
    colors[4].setup("color5", ofColor(100,100,140), ofColor(0,0),ofColor(255,255));
    colors[5].setup("color6", ofColor(100,100,140), ofColor(0,0),ofColor(255,255));
    
    xPosition[0].setup("x1", 10, 0, ofGetWidth());
    xPosition[1].setup("x2", 10, 0, ofGetWidth());
    xPosition[2].setup("x3", 10, 0, ofGetWidth());
    xPosition[3].setup("x4", 10, 0, ofGetWidth());
    xPosition[4].setup("x5", 10, 0, ofGetWidth());
    xPosition[5].setup("x6", 10, 0, ofGetWidth());
    
    yPosition[0].setup("y1", 10, 0, ofGetHeight());
    yPosition[1].setup("y2", 10, 0, ofGetHeight());
    yPosition[2].setup("y3", 10, 0, ofGetHeight());
    yPosition[3].setup("y4", 10, 0, ofGetHeight());
    yPosition[4].setup("y5", 10, 0, ofGetHeight());
    yPosition[5].setup("y6", 10, 0, ofGetHeight());
    
    radius[0].setup("r1", 10, 0, 500);
    radius[1].setup("r2", 10, 0, 500);
    radius[2].setup("r3", 10, 0, 500);
    radius[3].setup("r4", 10, 0, 500);
    radius[4].setup("r5", 10, 0, 500);
    radius[5].setup("r6", 10, 0, 500);
    
    on_off[0].setup("On 1", false);
    on_off[1].setup("On 2", false);
    on_off[2].setup("On 3", false);
    on_off[3].setup("On 4", false);
    on_off[4].setup("On 5", false);
    on_off[5].setup("On 6", false);
    
    for(int i = 0; i < 6; i++)
    {

        gui1.add(&xPosition[i]);
        gui1.add(&yPosition[i]);
        gui1.add(&radius[i]);
        gui1.add(&colors[i]);
    }
    gui1.minimizeAll();
    gui1.loadFromFile("settings.xml");
    gui1.setPosition(0,2);
    gui2.setPosition(200, 0);
    gui2.setSize(1000, 1000);
    hGui = false;
    hTimeline = true;
    ofSetCircleResolution(100);
    
    for(int i = 0; i < 8; i++)
    {
       addTimeline();
    }
}

void testApp::onToggle( const void * sender, bool & value )
{
    ofParameter<bool> * p = ( ofParameter<bool> * ) sender;
    string toggleName = p->getName();
    
    vector<string> whichLight = ofSplitString(toggleName, "_");
    if(value)
    {
        lights[ofToFloat(whichLight[1])-1]->addTimeline(sublines[ofToFloat(whichLight[2])]);
    }
    else
    {
        lights[ofToFloat(whichLight[1])-1]->removeTimeline(sublines[ofToFloat(whichLight[2])]->getName());
    }
}
void testApp::addAnimationToggle(int timeline)
{
    for(int i = 0; i < 6; i++)
    {
        ofxToggle* aux = new ofxToggle;
        string bla ="toggle_" + ofToString(i+1) + "_" + ofToString(timeline);
        aux->setup(bla, false);
        aux->addListener( this, &testApp::onToggle );

        animLight[i].push_back(aux);
        gui2.add(animLight[i].back());
        gui2.getToggle(bla).setPosition(gui2.getPosition().x + i * 120, gui2.getPosition().y + timeline * 30);
    }
}
void testApp::addTimeline()
{
    ofxTimeline* t = new ofxTimeline();
    t->setup();
    t->setName("timeline"+ofToString(sublines.size()));
    t->setOffset(ofVec2f(280 ,103 * (sublines.size())));
    t->setSpacebarTogglePlay(false);
    t->setDurationInFrames(400);
    t->addCurves("X", ofRange(0, ofGetWidth()));
    t->addCurves("Y", ofRange(0, ofGetHeight()));
    t->setWidth(ofGetWidth() - 280);
    t->setLoopType(OF_LOOP_NORMAL);
    t->setHeight(103);
    addAnimationToggle(sublines.size());

    sublines.push_back(t);

}

//--------------------------------------------------------------
void testApp::update()
{
	ofBackground(0, 0, 0);
    
    for(int i = 0; i < lights.size(); i++)
    {
        if(lights[i]->isNextTimeline);
        {
            for(int j = 0; j < sublines.size(); j++)
            {
                lights[i]->updateTimeline(sublines[j]);
            }
        }
    }
    
    /*for(int i = 0; i < 6; i++)
    {
        if(on_off[i])
            lightsOn[i] = true;
        
    }*/
    
}

void testApp::updateLights(ofEventArgs & args)
{

    
  /*  for(int i = 0; i < 6; i++)
    {
        if(sublines[i]->getIsPlaying())
        {
            on_off[i] = true;
            xPosition[i] = sublines[i]->getValue("X");
            yPosition[i] = ofGetHeight() - sublines[i]->getValue("Y");
        }
    }
    
    for(int i = 0; i < 6; i++)
    {
        if(on_off[i])
        {
            lightsOn[i] = true;
        }
        
    }*/
   
}
//--------------------------------------------------------------
void testApp::draw()
{
    ofFill();
    ofSetColor(255,0,0);
    
    for(int i = 0; i < lights.size(); i++)
    {
        ofSetColor(colors[i]);
        if(lights[i]->isRunning)
            ofCircle(lights[i]->getPos(), radius[i]);
    }
}

void testApp::drawGui(ofEventArgs & args)
{
    ofBackground(0, 0, 0);
    if(!hGui)
    {
        gui1.draw();
        gui2.draw();
    }
    if(sublines.size() > 0 && !hTimeline)
    {
        //sublines[0]->setOffset(timeline.getBottomLeft());
    
    //we need to offset all the timelines below the one above it, with a 10 pixel buffer
        for(int i = 0; i < sublines.size(); i++)
        {
            if(i != 0)
            {
                sublines[i]->setOffset(sublines[i-1]->getBottomLeft() + ofVec2f(0, 10));
            }
            sublines[i]->draw();
        }
    }
}


void testApp::dealWithKey(ofKeyEventArgs & args)
{
    bool press;
    if (args.type == args.Pressed)
        press = true;
    
    if(args.type == args.Released)
        press = false;
    
    int key = args.key;
    int bla = 0;
    
    switch (key){
        case '1':
            if(press)
            {
                if(lights[0]->isRunning)
                    lights[0]->stopAnimation();
                else
                    lights[0]->runAnimation();
                
            }
            break;
        case '2':
            if(press)
            {
                if(lights[1]->isRunning)
                    lights[1]->stopAnimation();
                else
                    lights[1]->runAnimation();
                
            }
            break;
        case '3':
            if(press)
            {
                if(lights[2]->isRunning)
                    lights[2]->stopAnimation();
                else
                    lights[2]->runAnimation();
                
            }
            break;
        case '4':
            if(press)
            {
                if(lights[3]->isRunning)
                    lights[3]->stopAnimation();
                else
                    lights[3]->runAnimation();
                
            }
            break;
        case '5':
            if(press)
            {
                if(lights[4]->isRunning)
                    lights[4]->stopAnimation();
                else
                    lights[4]->runAnimation();
                
            }
            break;
        case '6':
            if(press)
            {
                if(lights[5]->isRunning)
                    lights[5]->stopAnimation();
                else
                    lights[5]->runAnimation();
                
            }
            break;
        case 'h':
            if(press)
            {
                hGui = !hGui;
                hTimeline = !hGui;
            }
            if(hGui)
                gui1.unregisterMouseEvents();
            else
                gui1.registerMouseEvents();
            
            if(hTimeline)
            {
                for(int i = 0; i < sublines.size(); i++)
                {
                    sublines[i]->disable();
                }
            }
            else
            {
                for(int i = 0; i < sublines.size(); i++)
                {
                    sublines[i]->enable();
                }
            }
            break;
    }

}
//--------------------------------------------------------------
void testApp::keyPressed(int key)
{
}

void testApp::exit()
{
    gui1.saveToFile("settings.xml");
    gui2.saveToFile("settings.xml");

}
//--------------------------------------------------------------
void testApp::keyReleased(int key){
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

