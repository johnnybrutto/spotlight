//
//  timelineEditor.hpp
//  emptyExample
//
//  Created by Joao Freire on 8/11/16.
//
//

#ifndef timelineEditor_hpp
#define timelineEditor_hpp

#include <stdio.h>
#include "ofxTimeline.h"

class timelineEditor
{
public:
    void newTimeline(string name, int duration);
    void editTimeline(string name);
    void deleteTimeline(string name);
    
private:
    map <string, ofxTimeline> * timelines;
    ofxTimeline auxTImeline;
    
};

#endif /* timelineEditor_hpp */
