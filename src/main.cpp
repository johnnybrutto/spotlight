
#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"
#include "ofAppGLFWWindow.h"


//========================================================================
int main( ){

    ofGLFWWindowSettings settings;
    settings.width = 1920;
    settings.height = 1280;
    settings.windowMode = OF_FULLSCREEN;

    settings.setPosition(ofVec2f(1920,0));
    settings.resizable = false;
   // settings.windowMode = OF_FULLSCREEN;
    shared_ptr<ofAppBaseWindow> mainWindow = ofCreateWindow(settings);
    
    ofGLFWWindowSettings settings2;
    
    settings2.width = 1440;
    settings2.height = 900;
    settings2.setPosition(ofVec2f(0,0));
    settings2.resizable = false;
    shared_ptr<ofAppBaseWindow> guiWindow = ofCreateWindow(settings2);
    guiWindow->setVerticalSync(false);

    shared_ptr<testApp> mainApp(new testApp);
    mainApp->setup();
    mainApp->setupGui();
    ofAddListener(guiWindow->events().draw,mainApp.get(),&testApp::drawGui);
    ofAddListener(guiWindow->events().update,mainApp.get(),&testApp::updateLights);
    ofAddListener(guiWindow->events().keyPressed, mainApp.get(), &testApp::dealWithKey);
    ofAddListener(guiWindow->events().keyReleased, mainApp.get(), &testApp::dealWithKey);


 //   ofAddListener(guiWindow->events().keyPressed, mainApp.get(), &testApp::dealWithKey);
    
    ofRunApp(mainWindow, mainApp);
    ofRunMainLoop();


}
